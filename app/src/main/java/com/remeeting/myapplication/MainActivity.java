package com.remeeting.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import butterknife.Bind;

public class MainActivity extends AppCompatActivity {
	public static final String LogTag = "MainActivity";

	Fragment [] pages = new Fragment [] {
			new ExampleFragment(),
			new ExampleFragment(),
			new ExampleFragment(),
			new ExampleFragment(),
	};

	@Bind(R.id.textView1)
	TextView textView1;

	@Bind(R.id.textView2)
	TextView textView2;

	@Bind(R.id.viewPager)
	ViewPager viewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Bundle args = new Bundle();
		for (int i = 0; i < pages.length; ++i) {
			args.putInt("id", i);
			pages[i].setArguments(args);
		}

		viewPager.setAdapter(new ScreenSlidePagerAdapter(getSupportFragmentManager(), pages));
	}

	/**
	 * Not required, but clean.
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		BusProvider.getInstance().unregister(this);
	}

	public void onUXEvent(UXEvent event) {
		Log.d(LogTag, event.toString());

		if (event.payload != null)
			textView2.setText(event.payload.toString());
	}

	/**
	 * This is a pager that uses fixed number of fragments.
	 */
	private static class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		private Fragment [] pages;

		public ScreenSlidePagerAdapter(FragmentManager fm, Fragment [] pages) {
			super(fm);
			this.pages = pages;
		}

		@Override
		public Fragment getItem(int position) {
			return pages[position];
		}

		@Override
		public int getCount() {
			return pages.length;
		}
	}
}
